package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import java.nio.file.attribute.UserPrincipalNotFoundException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "Password");
    }

    @Test
    public void testLoginSuccess() throws UserNotFoundException,LoginFailedException {
        User user = loginManager.login("Testuser1", "Password");
        assertThat(user.getUsername(), is("Testuser1"));
        assertThat(user.getPassword(), is("Password"));
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginWrongPassword() throws UserNotFoundException,LoginFailedException {
        User user = loginManager.login("Testuser1", "1234");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser() throws UserNotFoundException, LoginFailedException {
        User user = loginManager.login("iniad", "Password");
    }

}